#!/usr/bin/env python3

import sys

read = sys.stdin.readlines()
read = [line.split('\n')[0] for line in read]

for sequence in read:
    found = False
    counter = 1
    series = ""
    index = 0
    dropped = 0
    while not found:
        # Заполняем последовательность S до длины не меньше, чем A
        while len(series) < len(sequence):
            series += str(counter)
            counter += 1
        # Ищем вхождение
        index = series.find(sequence)
        if index >= 0:
            found = True
        # Если не найдено, то отрезаем первый символ последовательности S,
        # и запоминаем сколько уже отрезали
        else:
            series = series[1:]
            dropped += 1
    print(index + dropped + 1, file=sys.stdout)