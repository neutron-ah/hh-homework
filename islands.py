#!/usr/bin/env python3

import sys
import copy

read = sys.stdin.readlines()
read = [line.split('\n')[0] for line in read]
islands_count = int(read.pop(0))
islands = list()


def add_island():
    island = list()
    size = read.pop(0).split()
    h = int(size[0])
    w = int(size[1])
    while h > 0:
        row = read.pop(0).split()
        row = [int(i) for i in row]
        island.append(row)
        h -= 1
    return island


for k in range(islands_count):
    islands.append(add_island())

path = list()


# Поиск выхода воды к морю с заданной клетки и высоты.
# Возвращает True или False
def find_sea(island: list, x: int, y: int, cell_level: int):
    path.append([x, y])
    # Перебираем соседние клетки
    for i, j in [[x - 1, y], [x + 1, y], [x, y - 1], [x, y + 1]]:
        # Если мы уже проверяли эту клетку, то пропускаем ее
        if [i, j] in path:
            continue
        # Если соседняя клетка не выше текущего уровня...
        if island[i][j] <= cell_level:
            # ...и она является берегом, то выход к морю найден
            if i == 0 or j == 0 or i == len(island) - 1 or j == len(island[i]) - 1:
                return True
            # ...и она НЕ явлется берегом, то проверяем её рекурсивно
            else:
                if find_sea(island, i, j, cell_level):
                    return True
    # Если выход не нашелся, то False
    return False


for island in islands:
    # Делаем полную копию острова без воды
    original_island = copy.deepcopy(island)
    # Перебираем все клетки, которые не являются берегом
    for i in range(1, len(island) - 1):
        for j in range(1, len(island[i]) - 1):
            # Постепенно поднимаем в каждой клетке уровень воды...
            for water_level in range(island[i][j], 1001):
                # ...пока она не начнет выливаться в море
                if find_sea(island, i, j, water_level):
                    island[i][j] = water_level
                    path = list()
                    break
    water = 0
    for i in range(1, len(island) - 1):
        for j in range(1, len(island[i]) - 1):
            # Считаем количество воды
            water += island[i][j] - original_island[i][j]
    print(water, file=sys.stdout)